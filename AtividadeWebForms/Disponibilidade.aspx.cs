﻿using System;
using System.Text;
using System.Web.UI.WebControls;

namespace AtividadeWebForms
{
    public partial class Disponibilidade : System.Web.UI.Page
    {
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            StringBuilder diasSelecionados = new StringBuilder();
            foreach (ListItem item in chkDiasSemana.Items)
            {
                if (item.Selected)
                {
                    diasSelecionados.Append(item.Text).Append("<br />");
                }
            }
            lblDiasSelecionados.Text = "Dias selecionados: <br />" + diasSelecionados.ToString();
        }
    }
}