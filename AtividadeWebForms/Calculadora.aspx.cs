﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AtividadeWebForms
{
    public partial class Calculadora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (IsValidInput())
            {
                double num1 = Convert.ToDouble(txtNum1.Text);
                double num2 = Convert.ToDouble(txtNum2.Text);
                string operation = ddlOperation.SelectedValue;
                double result = 0;

                switch (operation)
                {
                    case "add":
                        result = num1 + num2;
                        break;
                    case "subtract":
                        result = num1 - num2;
                        break;
                    case "multiply":
                        result = num1 * num2;
                        break;
                    case "divide":
                        if (num2 != 0)
                        {
                            result = num1 / num2;
                        }
                        else
                        {
                            lblResult.Text = "Não é possível dividir por zero.";
                            return;
                        }
                        break;
                    default:
                        lblResult.Text = "Escolha uma operação válida.";
                        return;
                }

                lblResult.Text = "Resultado: " + result.ToString();
            }
        }

        private bool IsValidInput()
        {
            double num1, num2;
            if (!double.TryParse(txtNum1.Text, out num1) || !double.TryParse(txtNum2.Text, out num2))
            {
                lblResult.Text = "Por favor, insira números válidos.";
                return false;
            }
            return true;
        }
    }
}