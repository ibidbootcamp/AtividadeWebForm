﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculadora.aspx.cs" Inherits="AtividadeWebForms.Calculadora" %>
<link rel="stylesheet" href="StyleSheet1.css" />

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Calculadora</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Calculadora</h1>
        <div>
            <asp:TextBox ID="txtNum1" runat="server"></asp:TextBox>
            <asp:DropDownList ID="ddlOperation" runat="server">
                <asp:ListItem Text="+" Value="add" />
                <asp:ListItem Text="-" Value="subtract" />
                <asp:ListItem Text="*" Value="multiply" />
                <asp:ListItem Text="/" Value="divide" />
            </asp:DropDownList>
            <asp:TextBox ID="txtNum2" runat="server"></asp:TextBox>
            <asp:Button ID="btnCalculate" runat="server" Text="Calcular" OnClick="btnCalculate_Click" />
            <br /><br />
            <asp:Label ID="lblResult" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>