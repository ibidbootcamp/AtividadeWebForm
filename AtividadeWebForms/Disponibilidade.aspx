﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Disponibilidade.aspx.cs" Inherits="AtividadeWebForms.Disponibilidade" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Escolha de Disponibilidade</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet2.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class ="container">
            <h2>Escolha os dias da semana em que você estará disponível para trabalhar:</h2>
            <asp:CheckBoxList ID="chkDiasSemana" runat="server">
                <asp:ListItem Text="Domingo" Value="0" />
                <asp:ListItem Text="Segunda-feira" Value="1" />
                <asp:ListItem Text="Terça-feira" Value="2" />
                <asp:ListItem Text="Quarta-feira" Value="3" />
                <asp:ListItem Text="Quinta-feira" Value="4" />
                <asp:ListItem Text="Sexta-feira" Value="5" />
                <asp:ListItem Text="Sábado" Value="6" />
            </asp:CheckBoxList>
            <asp:Button ID="btnSubmit" runat="server" Text="Enviar" OnClick="btnSubmit_Click" />
            <br />
            <asp:Label ID="lblDiasSelecionados" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>